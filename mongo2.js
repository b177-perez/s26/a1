// Get the maximum price of fruits on sale in the origin "Philippines"

db.fruits.aggregate([
   { $unwind: "$origin" },
   { $match: 
      { 
         onSale: true, 
         origin: "Philippines"
      } 
   },
   { $group: 
      { 
         _id: "$origin", 
         maxPrice: { $max: "$price" } 
      } 
   }
]);
