// Create an average stock from fruits on sale on specific suppliers

db.fruits.aggregate([
   { $match: 
      { onSale: true } 
   },
   { $group: 
      { _id: "$supplier_id", 
      averageStock: 
         { $avg: "$stock" } 
      } 
   }
]);
